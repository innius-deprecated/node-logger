export const enum Level {
    debug = 0,
    info = 1 << 0,
    warning = 1 << 1,
    error = 1 << 2,
    fatal = 1 << 3
}
