import {Level} from "./Level";

export interface Logger {
    setLevel(level: Level | string): void;
    getLevel(): Level;
    withField(key: string, value: any): Logger;
    // Add multiple keys: x.WithFields({foo: 1, bar: 'baz'});
    withFields(object: any): Logger;

    debug(message: any): void;
    info(message: any): void;
    warn(message: any): void;
    error(message: any): void;
    fatal(message: any): void;
}
