import {assert} from "chai";
import {ScopedLogger, newScopedLogger} from "./ScopedLogger";
import {Level} from "./Level";

describe("ScopedLogger", () => {
    it("Should be able to configure a loglevel", () => {
        let l = newScopedLogger();
        assert.equal(l.getLevel(), Level.info);
        l.setLevel(Level.debug);
        assert.equal(l.getLevel(), Level.debug);
    });
    it("Should be able to create a child logger with loglevels that bubble up.", () => {
        let l = new ScopedLogger();
        let child = l.withField("foo", "bar");

        assert.equal(l.getLevel(), Level.info);
        assert.equal(child.getLevel(), Level.info);

        child.setLevel(Level.debug);

        assert.equal(l.getLevel(), Level.debug);
        assert.equal(child.getLevel(), Level.debug);
    });
    it("Should be able to configure an extra field and log those.", () => {
        let l = new ScopedLogger();
        l.withField("foo", "bar").info("abc");
    });
    it("Should be able to configure extra fields and log those.", () => {
        let l = new ScopedLogger();
        l.withFields({
            "foo": 42,
            "bar": 123,
            "baz": "quuk"
        }).info("abc");
    });
    it("Should be able to configure extra initial fields and log with those.", () => {
        let l = newScopedLogger({
            "name": "abc",
            "level": "def"
        });
        l.info("abc");
    });
    it("Should be able to configure the log level", () => {
        let l = newScopedLogger();
        assert.equal(Level.info, l.getLevel());
        l.setLevel(Level.debug);
        assert.equal(Level.debug, l.getLevel());
        l.setLevel("warning");
        assert.equal(Level.warning, l.getLevel());
    });
    it("should not blow up on null", () => {
        let l = newScopedLogger();
        l.info(null);
    });
    describe("log an object", () => {
        it("should invoke the toString method", () => {
            let called: boolean = false;
            let err = class {
                toString(): string {
                    called = true;
                    return "foo";
                }
            };
            let l = newScopedLogger();
            l.error(new err());
            assert.isTrue(called);
        });
    });
});
