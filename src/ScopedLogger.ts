import {Level} from "./Level";
import {Logger} from "./Logger";

export function newScopedLogger(properties: any = {}): Logger {
    return new ScopedLogger(null, properties);
}

export class ScopedLogger implements Logger {
    private level: Level;
    private parent: Logger;
    private properties: any;

    constructor(parent: Logger = null, properties: any = {}) {
        this.level = Level.info;
        this.parent = parent;
        this.properties = properties;
    }

    setLevel(level: Level | string): void {
        let l: Level = Level.info;
        if (typeof level === "string") {
            switch (level) {
                case "debug":
                    l = Level.debug;
                    break;
                case "info":
                    l = Level.info;
                    break;
                case "warning":
                    l = Level.warning;
                    break;
                case "error":
                    l = Level.error;
                    break;
                case "fatal":
                    l = Level.fatal;
                    break;
            }
        } else {
            l = <Level>level;
        }
        this.level = l;
        // bubble the change of loglevel upwards.
        if (this.parent) {
            this.parent.setLevel(level);
        }
    }

    getLevel(): Level {
        return this.level;
    }

    withField(key: string, value: any): Logger {
        var o = {};
        o[key] = value;
        return this.withFields(o);
    };

    withFields(object: any): Logger {
        return new ScopedLogger(this, Object.assign(object, this.properties));
    };

    private log(level: Level, _level: string, message: any): void {
        // Todo: implement a proper logging backend (winston or something)
        if (!message) {
            message = "";
        }
        if (level >= this.level) {
            let m = Object.assign(this.properties, {
                msg: message.toString ? message.toString() : message,
                level: _level
            });
            console.log(JSON.stringify(m));
        }
    };

    debug(message: any): void {
        this.log(Level.debug, "debug", message);
    }

    info(message: any): void {
        this.log(Level.info, "info", message);
    }

    warn(message: any): void {
        this.log(Level.warning, "warn", message);
    }

    error(message: any): void {
        this.log(Level.error, "error", message);
    }

    fatal(message: any): void {
        this.log(Level.fatal, "fatal", message);
    }
}
